from ete3 import Tree
from Bio import SeqIO
import re
import os

def aquire_targets(directory):
    print('Aquiring targets..')
    fileList = os.listdir(directory)
    alnfiles = [x for x in fileList if not "RAxML_labelledTree" in x.split(".")[0]]
    dirname=os.path.abspath(directory)
    alnfiles = [(os.path.join(dirname,x),os.path.join(dirname,'RAxML_labelledTree.'\
                 +os.path.basename(x)+'_FINAL.tre')) for x in alnfiles]
    return(alnfiles)
    
def fire_loic(tree,target_painter,in_group,tree_outfile):
    print('Spooling..')
    tree = Tree(tree,format = 1)
    tree.set_outgroup(target_painter)
    anc = tree.get_common_ancestor(in_group)
    tree.set_outgroup(anc)
    root = tree.get_tree_root()
    target = [y for y in root.children for node in y.get_leaves() if node.name == target_painter]
    tree.write(outfile="aaa")
    target = target[0].get_leaves()
    for node in target:
        if node.name in target_painter:
            target.remove(node)
    tips = tree.get_leaves()
    preserve = [tip for tip in tips if not tip in target]
    print('Firing')
    tree.prune(preserve,preserve_branch_length=True)
    tree.write(outfile=tree_outfile)
    return(tree)

def merge_dicts(*dict_args):
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result

def cleanup(tree_outfile,alnfile):
    seqs = SeqIO.to_dict(SeqIO.parse(alnfile,'fasta'))
    tips = Tree(tree_outfile,format = 1).get_leaves()
    tips = [x.name for x in tips]
    print tips[:20]
    keep = [seq for seq in seqs.keys() for tip in tips if seq in tip]
    print len(keep)
    with open(os.path.abspath(alnfile)+"outfile_2",'w') as outfile:
        for seq in keep:
            outfile.write(">%s\n%s\n"%(seqs[seq].description,
                                      seqs[seq].seq))
    return(seqs)
            
                
        
    
if __name__ == '__main__':
    ####USER DEFINED VARIABLES####
    directory = 'CHYT_tree'
    outgroupFile = 'OutgroupAnd_TwilightUYS_raxxed.fasta'
    target_painter = "62264_Trichoplax_GPR"
    in_group = ["249_Mnemiopsis_ML274438a","119_Homo_sapiens_RGRGo_RGR_Rgr"]
    ####END USER DEFINED VARIABLES####
    alnfiles = aquire_targets(directory)
    totalSeq = {}
    for tuples in alnfiles:
        with open(tuples[1],'r') as infile:
            x = infile.read()
        pat = re.compile('\[.*?\]') #remove species names from alignment
        x = re.sub(pat,"",x) #remove species names from alignment
        with open(tuples[1],'w') as outfile:
            outfile.write("%s"%x) #overwrite the alignment, now with no species names
        tree_outfile = tuples[0]+"newTree"
        fire_loic(tuples[1],target_painter,in_group,tree_outfile)
        seqdict = cleanup(tree_outfile,tuples[0])
        totalSeq = merge_dicts(seqdict,totalSeq)
    print totalSeq.keys()[:10]
    print os.path.join(os.path.abspath(directory),"CONCAT_outfile.fasta")
    with open(os.path.join(os.path.abspath(directory),"CONCAT_outfile.fasta"),'w') as outfile:
        for seq in totalSeq:
            outfile.write(">%s\n%s\n"%(totalSeq[seq].description,
                                      totalSeq[seq].seq))
        if outgroupFile != None:
            with open(outgroupFile,'r') as manualOg:
                for line in manualOg.read():
                    outfile.write(line)
