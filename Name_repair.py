from ete3 import Tree
import re
from Bio import SeqIO
import argparse
import os

#treeFile = "02072017_on_02062017/02062017_tree_deraxxed.tre"
#seqFile = "02072017_on_02062017/02062017_tree_deraxxed_fullNames.aln"
#direction = "to TREE"
#Lrange = 3
#Urange = 8

def makeoutput(seqNew,outFile):
    base = os.path.basename(seqNew).split(".")
    print base
    if outFile == ".":
        outFile = os.path.normpath(os.path.dirname(os.path.abspath(seqNew)))
    output = os.path.normpath(os.path.join(outFile,base[0]+"_Repaired"))
    return(output)

def main(seqFile,treeFile,direction="to TREE"):
    tree = Tree(treeFile,format=1)
    seqs = SeqIO.to_dict(SeqIO.parse(seqFile,'fasta'))
    tip_list = [t.name for t in tree.get_leaves()]
    accpull = re.compile('(.*?)_gi',re.IGNORECASE)
    accpull2 =  re.compile('([0-9]{7,}).*?',re.IGNORECASE)
    with open(seqFile,'r') as mask:
        seqO = mask.read().splitlines()
    for name in seqs.keys():
        match = re.search(accpull,name)
        if match is not None and match.group(1):
            lame = match.group(1).split("_")[-1].split('.')[0]
            if len(lame) > 4:
                x = [k for k in tip_list if lame in k]
        else:
            try:
                match = name.split(" ")[0]
                print "matched",name
            except:
                print "lost",name
                continue
            x = [k for k in tip_list if match in k]
        if len(x) > 1:
            print "In the loop"
            print x
            for i in x:
                z = 0
                match2 = re.search(accpull,i)
                if match2 is not None and match2.group(1):
                    match2 = match2.group(1).split("_")[-1].split('.')[0]
                    print match2
                    while z < len(x):
                        print "LOOPING",x
                        if match2 in x[z]:
                            print match2,"IS IN", x[z]
                            z+=1
                            continue
                        x.remove(i)
                        z+=1
            print "KEPT ",x
            print ""
        if x and direction == "to ALN":
            seqs[x[0]] = seqs.pop(name)
        if x and direction == "to TREE":
            try:
                tip = tree.get_leaves_by_name(x[0])
                tip[0].name = name
            except:
                print x
                print tip
    if direction == "to ALN":
        with open(output,'w') as outFile:
            for k,v in seqs.items():
                #if '=' in k: print k
                outFile.write(">%s\n%s\n"%(k,v.seq))
    if direction == "to TREE":
        tree.write(format=1,outfile="NEW_TREE")
    output = makeoutput(seqFile,".")
#main(seqFile,treeFile)

if __name__ == "__main__":
    pass
        
